# README #

Aplicación que permite corregir el problema de **BOX** cuando por problemas de conflictos renombra las carpetas y vuelve a descargar la carpeta y la información.

Un ejemplo de este problema es el siguiente:

* En el repositorio de Box se tiene respalda una carpeta
*Nombre_Carpeta*

* Al momento de sincronizar Box detecta que la carpeta ya existe, así que la renombra usando el correo de la cuenta de Box y descarga la carpeta con su información, quedando de la siguiente forma:

*Nombre_Carpeta*

*Nombre_Carpeta (correo@dominio.com)*

La aplicación mueve todo el contenido a las carpetas originales, sino existe las crea y borra la carpeta que contiene el correo en su nombre

### What is this repository for? ###

* Aplicación que corrige problema de Box al crear nuevas carpetas por conflictos
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Preguntas o dudas?... dejen un mensaje por aquí: jevc05@gmail.com