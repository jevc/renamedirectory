﻿using CtrlAdminArchivos;
using RenameFoldersBox.Models;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace RenameFoldersBox.Controller
{
    public class CtrlRenameFoldersBox
    {    
        #region Atributos

        Carpetas ctrlCarpetas;
      
        #endregion
    
        #region Constructores

        /// <summary>
        /// Constructor base del Controlador
        /// </summary>
        public CtrlRenameFoldersBox()
        {
            // crea una entidad de la Configuración
            Configuracion = new Configuracion();            
        }
        #endregion
    
        #region Propiedades

        /// <summary>
        /// Propiedad que contiene la entidad de Configuración 
        /// de la apliación
        /// </summary>
        public Configuracion Configuracion { get; internal set; }

        #endregion
    
        #region Metodos
                     
        /// <summary>
        /// Método que permite mostrar una ventana 
        /// para seleccionar la carpeta que se desea procesar
        /// </summary>
        public void ObtenerRutaCarpeta()
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                // especifica que no se muestre la opción para crear nuevas carpetas
                dlg.ShowNewFolderButton = false;

                // crea el objeto que obtiene el resultado de selección de la ventana
                DialogResult result = dlg.ShowDialog();

                // valida si el usuario seleccionó una carpeta
                if (result == System.Windows.Forms.DialogResult.OK)
                    // asigna la ruta seleccionada a la propiedad de la Configuración
                    Configuracion.RutaCarpeta = dlg.SelectedPath;
            }
        }
        /// <summary>
        /// Método que se encarga de realizar el procesamiento
        /// de creación de carpetas y copiado de su información,
        /// así como la eliminación de las carpetas "replicadas"
        /// </summary>
        public void ProcesarCarpetas()
        {
            Configuracion.Mensajes = string.Empty;

            var listaCarpetasDuplicadas = new List<string>();                          

            // filtra las carpetas que contienen entre su nombre el correo que se capturó en la pantalla
            listaCarpetasDuplicadas.AddRange(ctrlCarpetas.ListaCarpetasInternas.Where(c => c.Contains(Configuracion.Correo)));
            
            // obtiene el total de las carpetas duplicadas para asignarla a la propiedad
            // que establece el máximo valor al progressbar de la vista
            Configuracion.TotalCarpetasDuplicadas = listaCarpetasDuplicadas.Count;
            
            // recorre la lista de las carpetas duplicadas
            foreach (var rutaCarpDuplicada in listaCarpetasDuplicadas)
            {
                // invoca la funcionalidad para procesar la carpeta duplicada
                // y verifica si se ejecutó correctamente
                if (!ProcesarCarpeta(rutaCarpDuplicada))
                    break;

                // incrementa la propiedad que indica el total de carpetas que han sido procesadas
                Configuracion.TotalCarpetasProcesadas++;
            }

            // verifica si todas las carpetas fueron procesadas correctamente
            if (Configuracion.TotalCarpetasProcesadas == listaCarpetasDuplicadas.Count)
                Configuracion.Mensajes = "Se han actualizado las carpetas correctamente";
        }
        /// <summary>
        /// Método que permite procesar una carpeta en especifico
        /// </summary>
        /// <param name="rutaCarpDuplicada">Ruta de la carpeta a procesar</param>
        /// <returns>Valor que indica si se realizó correctamente el procesamiento de la carpeta</returns>
        private bool ProcesarCarpeta(string rutaCarpDuplicada)
        {            
            // obtiene la ruta de la carpeta pero si el correo
            var rutaCarpetaOriginal = rutaCarpDuplicada.Replace(string.Format("({0})", Configuracion.Correo), string.Empty);
            string msgError;

            //valida si existe una carpeta similar pero sin el correo en su nombre
            if (!ctrlCarpetas.EsRutaCarpetaValida(rutaCarpetaOriginal))
                // sino existe invoca la creación de la carpeta 
                ctrlCarpetas.Crear(rutaCarpetaOriginal);

            // invoca funcionalidad para mover la información de la carpeta duplicada
            // y pasarla a la carpeta que tiene el nombre correctamente
            msgError = ctrlCarpetas.MoverInformacion(rutaCarpDuplicada, rutaCarpetaOriginal);

            // verifica si existió algun error al mover la información
            if (!string.IsNullOrEmpty(msgError))
            {
                // actualiza la propiedad que permite visualizar los mensajes en la vista
                Configuracion.Mensajes = msgError;                 
                return false;
            }
                
            // invoca funcionalidad para borrar la carpeta duplicada
            msgError = ctrlCarpetas.Borrar(rutaCarpDuplicada);

            if (!string.IsNullOrEmpty(msgError))
            {
                Configuracion.Mensajes = msgError;
                return false;
            }
                
            return true;
        }
        /// <summary>
        /// Método que permite verificar si los datos capturados
        /// en la vista son validos para realizar el procesamiento de la carpeta seleccionada
        /// </summary>
        public void ValidarDatos()
        {
            Configuracion.Mensajes = string.Empty;

            // crea el nuevo controlador que permite administrar las carpetas
            ctrlCarpetas = new Carpetas(Configuracion.RutaCarpeta);

            // realiza validaciones y asigna mensaje de validación en caso de ser requerido
            if (string.IsNullOrEmpty(Configuracion.RutaCarpeta))
                Configuracion.Mensajes = "* Debe seleccionar la ruta de la carpeta de Box";
            else if (string.IsNullOrEmpty(Configuracion.Correo))
                Configuracion.Mensajes += "* Debe proporcionar la cuenta de correo";            
            else {
                CtrlGeneral ctrlGral = new CtrlGeneral();

                if (!ctrlGral.EsCorreoValido(Configuracion.Correo))
                    Configuracion.Mensajes += "* La cuenta de correo no es valida";
            }            
        }

        #endregion
    }
}
