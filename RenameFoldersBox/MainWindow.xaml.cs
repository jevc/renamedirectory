﻿using RenameFoldersBox.Controller;
using System.Collections.Generic;
using System.Windows;

namespace RenameFoldersBox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Atributos

        CtrlRenameFoldersBox _ctrl;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor de la vista
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // invoca la inicialización del controlador principal
            InicializarControlador();

            // asigna la entidad que proporciona la información a la vista
            this.DataContext = _ctrl.Configuracion;
        }

        #endregion

        #region Metodos

        /// <summary>
        /// Método que inicializa al controlador principal de la aplicación
        /// </summary>
        private void InicializarControlador()
        {
            if (_ctrl == null)
                _ctrl = new CtrlRenameFoldersBox();
        }
        #endregion

        #region Delegados

        /// <summary>
        /// Delegado que se ejecuta cuando se selecciona la opción de Procesar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProcesar_Click(object sender, RoutedEventArgs e)
        {
            // asigna la cuenta de correo capturada en el formulario 
            // a la entidad de Configuración del controlador
            _ctrl.Configuracion.Correo = txtCorreo.Text.ToString();

            // verifica si los datos capturados en el formulario son validos
            _ctrl.ValidarDatos();

            // valida si existen mensajes de validaciones a mostrar
            if (!string.IsNullOrEmpty(_ctrl.Configuracion.Mensajes))
                return;
            
            progressBar.Visibility = Visibility.Visible;

            // invoca el procesamiento de las carpetas
            _ctrl.ProcesarCarpetas();

            progressBar.Visibility = Visibility.Hidden;           
        }

        protected void btnSeleccionarCarpeta_Click(object sender, RoutedEventArgs e)
        {
            // verifica si el controlador existe
            if (_ctrl == null)
                InicializarControlador();

            // invoca funcionalidad para permitir seleccionar una carpeta del equipo
            _ctrl.ObtenerRutaCarpeta();            
        }

        #endregion
    }
}
