﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RenameFoldersBox.Models
{
    public class Configuracion : INotifyPropertyChanged
    {
        #region Atributos

        private string rutaCarpeta;
        private string correo;
        private string mensajes;
        private int totalCarpetasDuplicadas;
        private int totalCarpetasProcesadas;

        #endregion

        #region Propiedades

        public string RutaCarpeta
        {
            get { return this.rutaCarpeta; }
            set
            {
                if (this.rutaCarpeta != value)
                {
                    this.rutaCarpeta = value;
                    this.NotifyPropertyChanged("RutaCarpeta");
                }
            }
        }

        public string Correo {
            get { return this.correo; }
            set
            {
                if (this.correo != value)
                {
                    this.correo = value;
                    this.NotifyPropertyChanged("Correo");
                }
            }
        }

        public string Mensajes
        {
            get { return this.mensajes; }
            set
            {
                if (this.mensajes != value)
                {
                    this.mensajes = value;
                    this.NotifyPropertyChanged("Mensajes");
                }
            }
        }

        public int TotalCarpetasDuplicadas
        {
            get { return this.totalCarpetasDuplicadas; }
            set
            {
                if (this.totalCarpetasDuplicadas != value)
                {
                    this.totalCarpetasDuplicadas = value;
                    this.NotifyPropertyChanged("TotalCarpetasDuplicadas");
                }
            }
        }

        public int TotalCarpetasProcesadas
        {
            get { return this.totalCarpetasProcesadas; }
            set
            {
                if (this.totalCarpetasProcesadas != value)
                {
                    this.totalCarpetasProcesadas = value;
                    this.NotifyPropertyChanged("TotalCarpetasProcesadas");
                }
            }
        }

        #endregion

        #region Eventos

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion

    }
}
